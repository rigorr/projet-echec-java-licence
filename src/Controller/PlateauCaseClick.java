package Controller;

import java.awt.event.*;

import javax.swing.SwingWorker;

import Model.Model;
import Vue.CaseButton;

public class PlateauCaseClick implements ActionListener {

	private Model model;
	
	public PlateauCaseClick(Model model) {
		this.model=model;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		

		Object obj = e.getSource();
		CaseButton caseClicked = (CaseButton)obj;
		SwingWorker sw1 = new SwingWorker() 
        {
  
            @Override
            protected String doInBackground() throws Exception 
            {
        		model.verifyTurn(caseClicked.getButtonID());
				return "DUMMY";
            }
            
        };
        sw1.execute();
	}

}
