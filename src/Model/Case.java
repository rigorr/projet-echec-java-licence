package Model;

import java.io.Serializable;

import javax.swing.Icon;

public class Case implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5404113814949312645L;
	private Piece pieceActuelle;
	private int caseID;
	private boolean isSelected;
	private Icon icon;
	private boolean isValidPlay;
	
	
	public Case(int caseID) {
			
		this.caseID=caseID;
	}

	public Case(Case caseToClone) {
		
		// C'est hyper fastidieux mais pour l'IA en arbre j'ai besoin
		//  de tout cloner et j'ai pas le temps de chercher plus élégant
		if(caseToClone.getPieceActuelle() instanceof Pion) {
			this.pieceActuelle = new Pion(caseToClone.getPieceActuelle().getCouleur(),"pion");
			this.pieceActuelle.setSelfCase(this);
		}
		else if(caseToClone.getPieceActuelle() instanceof Cavalier) {
			this.pieceActuelle = new Cavalier(caseToClone.getPieceActuelle().getCouleur(),"cavalier");
			this.pieceActuelle.setSelfCase(this);
		}
		else if(caseToClone.getPieceActuelle() instanceof Fou) {
			this.pieceActuelle = new Fou(caseToClone.getPieceActuelle().getCouleur(),"fou");
			this.pieceActuelle.setSelfCase(this);
		}
		else if(caseToClone.getPieceActuelle() instanceof Tour) {
			this.pieceActuelle = new Tour(caseToClone.getPieceActuelle().getCouleur(),"tour");
			this.pieceActuelle.setSelfCase(this);
		}
		else if(caseToClone.getPieceActuelle() instanceof Reine) {
			this.pieceActuelle = new Reine(caseToClone.getPieceActuelle().getCouleur(),"reine");
			this.pieceActuelle.setSelfCase(this);
		}
		else if(caseToClone.getPieceActuelle() instanceof Roi) {
			this.pieceActuelle = new Roi(caseToClone.getPieceActuelle().getCouleur(),"roi");
			this.pieceActuelle.setSelfCase(this);
		}
		
		this.caseID = caseToClone.caseID;
		this.isSelected = caseToClone.isSelected;
		this.icon = caseToClone.getIcon();
		// On peut set en dur ici ca sert seulement à montrer si la case est jouable
		// on se sert de ce constructeur uniquement pour construire les faux plateaux de l'IA
		this.isValidPlay = false;
	}

	public Piece getPieceActuelle() {
		return pieceActuelle;
	}


	public void setPieceActuelle(Piece pieceActuelle) {
		this.pieceActuelle = pieceActuelle;
	}


	public int getCaseID() {
		return caseID;
	}


	public void setCaseID(int caseID) {
		this.caseID = caseID;
	}


	public boolean isSelected() {
		return isSelected;
	}


	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}


	public Icon getIcon() {
		return icon;
	}


	public void setIcon(Icon icon) {
		this.icon = icon;
	}


	public boolean isValidPlay() {
		return isValidPlay;
	}


	public void setValidPlay(boolean isValidPlay) {
		this.isValidPlay = isValidPlay;
	}
	
	
	
}
