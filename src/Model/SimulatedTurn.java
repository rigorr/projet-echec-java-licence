package Model;

import java.util.ArrayList;

public class SimulatedTurn {

	private int poids;
	private ArrayList<Case> plat;
	private ArrayList<SimulatedTurn> ennemyMoves;
	private int firstClick,secondClick;
	
	
	public SimulatedTurn(ArrayList<Case> plat) {
		
		this.plat=plat;
		ennemyMoves = new ArrayList<SimulatedTurn>();
		
	}


	public int getPoids() {
		return poids;
	}


	public void setPoids(int poids) {
		this.poids = poids;
	}


	public ArrayList<Case> getPlat() {
		return plat;
	}


	public void setPlat(ArrayList<Case> plat) {
		this.plat = plat;
	}


	public int getFirstClick() {
		return firstClick;
	}


	public void setFirstClick(int firstClick) {
		this.firstClick = firstClick;
	}


	public int getSecondClick() {
		return secondClick;
	}


	public void setSecondClick(int secondClick) {
		this.secondClick = secondClick;
	}


	public ArrayList<SimulatedTurn> getEnnemyMoves() {
		return ennemyMoves;
	}


	public void setEnnemyMoves(ArrayList<SimulatedTurn> ennemyMoves) {
		this.ennemyMoves = ennemyMoves;
	}
	
	
	
}
