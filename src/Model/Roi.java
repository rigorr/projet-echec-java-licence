package Model;

import java.util.ArrayList;

public class Roi extends Piece{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6975610874974725248L;

	public Roi(String couleur, String nom) {
		
		super(couleur,nom);
		this.poids=1000;
	}

	public ArrayList<Integer> getPath(int selectedCaseIntValue, ArrayList<Case> cases){
		
		ArrayList<Integer> retList = new ArrayList<Integer>();
		
		// Ces valeurs ne doivent être ajouté que si on est pas sur le bord gauche
		if ( !leftBorder.contains(selectedCaseIntValue) ) {
			retList.add(selectedCaseIntValue - 1);
			retList.add(selectedCaseIntValue - 9);
			retList.add(selectedCaseIntValue + 7);
		}
		// Ces valeurs ne doivent être ajouté que si on est pas sur le bord droit
		if ( !rightBorder.contains(selectedCaseIntValue) ) {
			retList.add(selectedCaseIntValue + 1);
			retList.add(selectedCaseIntValue + 9);
			retList.add(selectedCaseIntValue - 7);	
		}
		// Les déplacement en haut et en bas ne posent pas de problème puisqu'on ne peut
		// cliquer en dehors du plateau
		retList.add(selectedCaseIntValue + 8);
		retList.add(selectedCaseIntValue - 8);

				
		// On enlève les valeurs hors du plateau, utile uniquement pour l'IA
		for(int i=0;i<retList.size();++i)
			if(retList.get(i)<0 || retList.get(i)>55) {
				retList.remove(i);
				i=0;
			}
		return retList;
	}
}
