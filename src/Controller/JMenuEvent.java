package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JMenuItem;

import Model.Model;
import Vue.MainFrame;

public class JMenuEvent implements ActionListener{
	
	private Model model;
	private MainFrame mf;
	private ObjectOutputStream oos; 
	private ObjectInputStream ois;
	
	
	public JMenuEvent(Model model, MainFrame mf) throws FileNotFoundException, IOException {
		this.model=model;
		this.mf=mf;
		File fich =  new File("save");

		// ouverture d'un flux sur un fichier
		oos =  new ObjectOutputStream(new FileOutputStream(fich));
		ois =  new ObjectInputStream(new FileInputStream(fich));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		//On regarde quel objet à appelé l'action
		Object obj = e.getSource();
				
		JMenuItem actionCaller = (JMenuItem)obj;
		
		if (actionCaller.getText().equals("Partie débutant")) {
				
			model.initPlateau();
			model.getJoueurs().get(1).setDifficulty(1);
			
		}
		else if (actionCaller.getText().equals("Partie facile")) {
			
			model.initPlateau();
			model.getJoueurs().get(1).setDifficulty(2);
			
		}
		else if (actionCaller.getText().equals("Partie intermédiaire")) {
			
			model.initPlateau();
			model.getJoueurs().get(1).setDifficulty(3);
			
		}
		else if (actionCaller.getText().equals("Sauvegarder")) {
			
            // serialisation de l'objet
			try {
				oos.writeObject(model);
				oos.flush();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		else if (actionCaller.getText().equals("Charger")) {
			
	
			model.initPlateau();
			Model m = new Model(mf);
			try {
				m = (Model)ois.readObject() ;
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			model.loadSave(m);
			
		}	
	}
}
