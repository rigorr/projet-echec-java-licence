package Vue;

import java.awt.Font;
import java.awt.GridLayout;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import java.awt.Component;
import java.awt.Dimension;

import Model.Joueur;

public class SidePanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6394078429838288235L;
	private  Timer timer;
	private JLabel activePlayerText;
	private JLabel timerLabel;
	private int actualTimeMS;
	private Joueur idlePlayer;
	private JPanel topPanel, botPanel;
	private MainFrame mf;
	
	public SidePanel(MainFrame mf) {
		
		super();
		this.setPreferredSize(new Dimension(300,200));
		this.setLayout(new GridLayout(2,0));
		this.mf=mf;
		
		// Les pièces blanches commencent toujours la partie
		activePlayerText = new JLabel("Joueur actif : BLANC");
		activePlayerText.setFont(new Font("Serif", Font.PLAIN, 25));
		
		// Chrono de 3mn 
		timerLabel = new JLabel("Temps restant: 3:00");
		timerLabel.setFont(new Font("Serif", Font.PLAIN, 25));

		
		// On créer 2 panels pour stocker le timer et le joueur actif
		//  afin de pouvoir centrer ces deux JLabels
		topPanel = new JPanel();
		botPanel = new JPanel();
		
		topPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		//topPanel.setAlignmentY(Component.CENTER_ALIGNMENT);
		botPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		//botPanel.setAlignmentY(Component.CENTER_ALIGNMENT);
		
		topPanel.add(activePlayerText);
		botPanel.add(timerLabel);
		
		this.add(topPanel);
		this.add(botPanel);
	
		
	}
	
	public void startTimer(Joueur idlePlayer) {
		
		timer = new Timer();
		// 3 mn en milisc
		actualTimeMS=180000000;
		
		this.idlePlayer=idlePlayer;
		
		timer.scheduleAtFixedRate(new TimerTask() {

	        public void run() {
	            updateTimerCount();
	        }
	    }, 1000, 1000);
		
	}
	
	public void setJoueurActif(String couleur) {
		
		activePlayerText.setText("Joueur actif : " + couleur);
		
	}
	
	private void updateTimerCount() {
		
		// Si le timer est a 1 seconde ici, one le passerai a 0,
		// donc on arrête tout et le joueur Idle est déclaré vainqueur
		if (actualTimeMS == 1000) {
			timer.cancel();
			mf.endGame(idlePlayer);
		}
		actualTimeMS-=1000;
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {

				// On affiche le 0 devant l'unité si le nombre de secondes est inférieur a 10
				if ((actualTimeMS / 1000) % 60 >= 10)
					timerLabel.setText("Temps restant: "+((actualTimeMS / (1000*60)) % 60)+":"+(actualTimeMS / 1000) % 60 );
				else timerLabel.setText("Temps restant: "+((actualTimeMS / (1000*60)) % 60)+":0"+(actualTimeMS / 1000) % 60 );
		    }
		  });

		
	}
	
	// Quand le joueur actif a joué, on cancel le timer actuel et on en relance un
	public void updateTimer(Joueur idlePlayer) {
		// 3 mn en milisc
		actualTimeMS=180000000;
		this.idlePlayer = idlePlayer;
		
	}
	
	public void stopTimer() {
		timer.cancel();
	}

	public Timer getTimer() {
		return timer;
	}

	public void setTimer(Timer timer) {
		this.timer = timer;
	}

	

}
