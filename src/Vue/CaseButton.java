package Vue;

import javax.swing.JButton;
import java.awt.Color;

// Cette classe sert uniquement a avoir un JButton avec un ID unique
// qui ne s'affiche par sur le plateau contrairement a la property "name" de JButton
public class CaseButton extends JButton{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5418195900736626144L;
	private int buttonID;
	private Color baseColor;
	
	public CaseButton(int buttonID) {
		this.buttonID=buttonID;
	}

	public int getButtonID() {
		return buttonID;
	}

	public void setButtonID(int buttonID) {
		this.buttonID = buttonID;
	}

	public Color getBaseColor() {
		return baseColor;
	}

	public void setBaseColor(Color baseColor) {
		this.baseColor = baseColor;
	}

}
