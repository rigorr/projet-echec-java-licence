package Model;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import Vue.MainFrame;
import Vue.PromoFrame;

public class Model implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4724820170521275560L;
	private transient MainFrame mf;
	private ArrayList<Case>cases;
	private ArrayList<Joueur>joueurs;
	private Case selectedCase, caseClicked;
	//Utilisés pour vérifier si un pion est promu
	private ArrayList<Integer> topBorder; 
	private ArrayList<Integer> bottomBorder;
	
	public Model(MainFrame mf) {
		
		this.mf=mf;
		//Initialisation de la liste de cases
		cases = new ArrayList<Case>();
		
		// Initialisation des joueurs
		joueurs = new ArrayList<Joueur>();	
		
		topBorder=new ArrayList<Integer>();
		bottomBorder=new ArrayList<Integer>();
		topBorder.addAll(Arrays.asList(0,1,2,3,4,5,6,7));
		bottomBorder.addAll(Arrays.asList(48,49,50,51,52,53,54,55));
		
	}
	
	public void initPlateau() {
		
		cases.clear();
		joueurs.clear();
		if(mf.getSidePanel().getTimer()!=null)
			mf.getSidePanel().stopTimer();
		
		
		joueurs.add(new Joueur("blanc"));
		joueurs.add(new AI("noir",this,joueurs.get(0)));
		// Le joueur blanc joue en premier
		joueurs.get(0).setActive(true);
		
		for (int i=0;i<7*8;++i) {
			Case caseIteration = new Case(i);
			
			// Les tours
			if (i == 0 || i == 7)
				caseIteration.setPieceActuelle(new Tour("noir","tour"));
			else if (i == 48 || i == 55)
				caseIteration.setPieceActuelle(new Tour("blanc","tour"));
			// Les pions
			else if (i >= 8 && i <= 15)
				caseIteration.setPieceActuelle(new Pion("noir","pion"));
			else if (i >= 40 && i <= 47)
				caseIteration.setPieceActuelle(new Pion("blanc","pion"));
			// Les cavaliers
			else if (i == 1 || i == 6)
				caseIteration.setPieceActuelle(new Cavalier("noir","cavalier"));
			else if (i == 49 || i == 54)
				caseIteration.setPieceActuelle(new Cavalier("blanc","cavalier"));
			// Les fous
			else if (i == 2 || i == 5)
				caseIteration.setPieceActuelle(new Fou("noir","fou"));
			else if (i == 50 || i == 53)
				caseIteration.setPieceActuelle(new Fou("blanc","fou"));
			// Les rois
			else if (i == 3)
				caseIteration.setPieceActuelle(new Reine("noir","reine"));
			else if (i == 51)
				caseIteration.setPieceActuelle(new Reine("blanc","reine"));
			// Les reines
			else if (i == 4)
				caseIteration.setPieceActuelle(new Roi("noir","roi"));
			else if (i == 52)
				caseIteration.setPieceActuelle(new Roi("blanc","roi"));
			
			// On donne la pièce au joueur correspondant
			if (caseIteration.getPieceActuelle()!=null) {
				if (caseIteration.getPieceActuelle().getCouleur()==joueurs.get(0).getCouleur())
					joueurs.get(0).getPiecesJoueur().add(caseIteration.getPieceActuelle());
				else joueurs.get(1).getPiecesJoueur().add(caseIteration.getPieceActuelle());
				
				// Bon la clairement je sens que c'est pas dingue, mais ca facilite beaucoup 
				// le jeu de l'IA qu'une pièce ai la case qui la contient
				caseIteration.getPieceActuelle().setSelfCase(caseIteration);
			}
			cases.add(caseIteration);
		}
		
		
		
		// On lance le timer
		mf.getSidePanel().startTimer(getIdlePlayer());
		
		// On set les images des pieces pour tout le monde
		for (int i=0;i<cases.size();++i) {
			if(cases.get(i).getPieceActuelle()!=null)
				setCaseIcon(cases.get(i));
			mf.rePaint(cases.get(i),i);
		}
		
	}
	
	public void setCaseIcon(Case caseToSet) {
		
		//On récupère l'image
		File imgFile = null;
			imgFile = new File(System.getProperty("user.dir")+"/images/"+caseToSet.getPieceActuelle().getName()+
					"_"+caseToSet.getPieceActuelle().getCouleur()+".png");
		BufferedImage img = null;
		try {
			img = ImageIO.read(imgFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//On stretch aux dimensions des cases -10 pour éviter de les faire coller aux bords
		Image tmpImg = img.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
		caseToSet.setIcon(new ImageIcon(tmpImg));
		
	}
	
	public void callForVueUpdate() {
		
		for (int i=0;i<cases.size();++i)
			mf.rePaint(cases.get(i),i);
		
	}
	
	
	public void verifyTurn(int buttonID) {
		
		
		boolean selectionAlreadyDone=false;
		
		// On vérifie d'abord si une case a déjà été selectionné auparavant
		for (int i=0;i<cases.size();++i)
			if (cases.get(i).isSelected()) {
				selectionAlreadyDone=true;
				break;
			}
			
		// On stocke la case sur laquelle on a clické
		caseClicked=cases.get(buttonID);
		
		// Si c'est le cas, on doit potentiellement gérer un mouvement de pièce
		// On vérifie si le mouvement est légal selon chaque type de pièce
		if (selectionAlreadyDone && mouvementLegal(selectedCase.getPieceActuelle(),caseClicked, selectedCase)) {
			
			
			//S'il y avait une piece de l'autre couleur, on l'enlève de la liste du joueur
			if (caseClicked.getPieceActuelle()!=null) {
				for (int i=0;i<getIdlePlayer().getPiecesJoueur().size();++i)
					if(getIdlePlayer().getPiecesJoueur().get(i)==caseClicked.getPieceActuelle())
						getIdlePlayer().getPiecesJoueur().remove(i);
				
			}

			// Le mouvement est légal, on donne la pièce a la case selectionnée
			caseClicked.setPieceActuelle(selectedCase.getPieceActuelle()); 
			caseClicked.setIcon(selectedCase.getIcon());
			// et on l'enlève de la case précédente
			selectedCase.setPieceActuelle(null);	
			selectedCase.setIcon(null);
					
			//On met a jour la case de la pièce
			caseClicked.getPieceActuelle().setSelfCase(caseClicked);
			//On vérifie si un pion est promu
			if(caseClicked.getPieceActuelle() instanceof Pion
					&& isInEnnemyLine(Integer.valueOf(caseClicked.getCaseID()), getActivePlayer().getCouleur())) {
				
				PromoFrame PF = new PromoFrame(caseClicked,getActivePlayer().getCouleur(),70);
				
				// On ajoute plusieurs évènements à la fermeture de la fenêtre de promotion
				PF.addWindowListener(new WindowAdapter() {				 
					@Override
					public void windowClosing(WindowEvent e) {
						// On recupère la sélection et on change la pièce actuelle par celle choisie
						caseClicked.setPieceActuelle(PF.getPieceAPromo().getPieceActuelle());
						caseClicked.setIcon(PF.getPieceAPromo().getIcon());
						//On update la vue a la fermeture de la fenêtre

						callForVueUpdate();

						mf.getSidePanel().updateTimer(getIdlePlayer());
					}		 
				});
				// On affiche la fenêtre$
				Thread T = new Thread(PF);
				T.start();
				try {
					T.join();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
			
			// On réinitialise la sélection
			for (int i=0;i<cases.size();++i) {
				cases.get(i).setSelected(false);
				cases.get(i).setValidPlay(false);
			}
			
			selectedCase=null;
			
			//On change le joueur actif
			swapActivePlayer();
			// On donne IdlePlayer car c'est lui qui sera affiché comme victorieux
			// au bout des 3 minutes
			mf.getSidePanel().updateTimer(getIdlePlayer());
			mf.getSidePanel().setJoueurActif(getActivePlayer().getCouleur().toUpperCase());

			// Si on est arrivés ici et que le joueur actif était humain, il faut faire jouer l'IA
			if(getActivePlayer() instanceof AI) {
				
				Thread T = new Thread(getActivePlayer());
				T.start();
				/*try {
					T.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			}		
			
		}
		// Sinon, on refait une sélection
		else {
			
			for (int i=0;i<cases.size();++i)
				// On sélectionne la case uniquement si une piece est présente dessus
				// et que l'on est propriétaire de la pièce selectionnée
				if(cases.get(i) == caseClicked 
					&& caseClicked.getPieceActuelle() != null
					&& getActivePlayer().getCouleur() == caseClicked.getPieceActuelle().getCouleur()) {
						//On a une case sélectionnée
						cases.get(i).setSelected(true);
						selectedCase=cases.get(i);
						
				}
				else cases.get(i).setSelected(false);
					
			if(selectedCase!=null) {
				// On veut afficher les coups possibles pour le joueur 
				ArrayList<Integer> validPaths = selectedCase.getPieceActuelle().getPath(selectedCase.getCaseID(), cases);
				for(int j=0;j<cases.size();++j)
					if(validPaths.contains(j) && 
							(cases.get(j).getPieceActuelle()==null ||
							 cases.get(j).getPieceActuelle().getCouleur() != getActivePlayer().getCouleur()))
						cases.get(j).setValidPlay(true);
					else cases.get(j).setValidPlay(false);
			}
		}
		// On demande a la vue de se mettre à jour
		callForVueUpdate();
		
		// On vérifie si le roi à été pris
		checkWinCondition();
	}
	
	public boolean mouvementLegal(Piece typePiece, Case caseClicked, Case selectedCase) {
			
		//Le mouvement est ok si la case sur laquelle on a cliqué est dans les possibilités
		//renvoyées par le pathbuilder, et que la case clickée contient soit rien, soit une pièce ennemie
		return( typePiece.getPath(selectedCase.getCaseID(), cases).contains(caseClicked.getCaseID())
				&& ( caseClicked.getPieceActuelle()==null 
				     || caseClicked.getPieceActuelle().getCouleur()!=selectedCase.getPieceActuelle().getCouleur() ) );
		
	}
	
	private Joueur getActivePlayer() {
		if(joueurs.get(0).isActive())
			return joueurs.get(0);
		else return joueurs.get(1);
	}
	
	private Joueur getIdlePlayer() {
		if(!joueurs.get(0).isActive())
			return joueurs.get(0);
		else return joueurs.get(1);
	}
	
	// On change le joueur actif
	private void swapActivePlayer() {
		joueurs.get(0).setActive(!joueurs.get(0).isActive());
		joueurs.get(1).setActive(!joueurs.get(1).isActive());
		
		//side.setJoueurActif(getActivePlayer().getCouleur().toString());
	}
	
	// On vérifie si quelqu'un a gagné au terme du tour
	private void checkWinCondition() {
		
		boolean hasKing = false;
		
		// On vérifie que le joueur actif a toujours son roi, sinon le joueur passif gagne
		for (int i=0;i<getActivePlayer().getPiecesJoueur().size();++i)
			if (getActivePlayer().getPiecesJoueur().get(i) instanceof Roi)
				hasKing=true;
		
		// Si pas de roi, on va afficher le gagnant et disable les boutons
		if(!hasKing) {
			mf.getSidePanel().stopTimer();
			mf.endGame(getIdlePlayer());
		}
	}
	
	// On vérifie si le pion doit être promu
	public boolean isInEnnemyLine(int caseID, String joueurCouleur) {
		// On se permet de ne pas caster Piece car on s'assure avant qu'il s'agit d'un pion
		if(joueurCouleur.equals("blanc"))
			return topBorder.contains(caseID);
		else return bottomBorder.contains(caseID);	
	}	
	
	
	public int calculPoids(ArrayList<Case> cases, Joueur activePlayer) {
		
		int poidsFinal=0;
		
		for(Case useCase : cases) {
			if(useCase.getPieceActuelle()!=null) {
				if(useCase.getPieceActuelle().getCouleur()==activePlayer.getCouleur())
					poidsFinal+=useCase.getPieceActuelle().getPoids();
				else poidsFinal-=useCase.getPieceActuelle().getPoids();
			}
			
		}
		
		return poidsFinal;
	}
	
	public ArrayList<Case> getCases() {
		return cases;
	}

	public void setCases(ArrayList<Case> cases) {
		this.cases = cases;
	}

	public ArrayList<Joueur> getJoueurs() {
		return joueurs;
	}

	public void setJoueurs(ArrayList<Joueur> joueurs) {
		this.joueurs = joueurs;
	}

	public MainFrame getMf() {
		return mf;
	}
	
	public void loadSave(Model m) {
		
		setCases(m.getCases());
		setJoueurs(m.getJoueurs());
		
		for(Joueur j : joueurs)
			if(j instanceof AI)
				((AI) j).setModel(this);
		
		callForVueUpdate();
	}
}
