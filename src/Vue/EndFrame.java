package Vue;

import java.awt.Dimension;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Model.Joueur;

public class EndFrame extends JDialog{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 82428177008178293L;
	private JLabel winLabel;
	private JPanel mainPanel;
	
	public EndFrame() {	
		
		super();
		this.setResizable(false);
		// On veut éviter d'agir sur le plateau pendant ce temps
		this.setModal(true);
		
		this.mainPanel=new JPanel();
		mainPanel.setPreferredSize(new Dimension(300,200));
		
		winLabel = new JLabel();
		
		mainPanel.add(winLabel);
		
		getContentPane().add(mainPanel);
		
	}
	
	
	public void endGame(Joueur winner) {
		
		winLabel.setText("Le joueur "+ winner.getCouleur() +" remporte la partie.");
	
		this.pack();
		this.setVisible(true);
		
	}
	
	
}
