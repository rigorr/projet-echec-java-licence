package Model;

import java.util.ArrayList;

public class Tour extends Piece{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1071490459304532128L;

	public Tour(String couleur, String nom) {
		
		super(couleur,nom);
		this.poids=30;
	}
	
	public ArrayList<Integer> getPath(int selectedCaseIntValue, ArrayList<Case> cases){
		
		ArrayList<Integer> retList = new ArrayList<Integer>();
		
		int tempValue = selectedCaseIntValue;
		
		// tant qu'on est pas sur le bord, on peut monter
		while(!topBorder.contains(tempValue)) {
			// Si on arrive sur une case non vide
			if(cases.get(tempValue-=8).getPieceActuelle()!=null) {
				// Si elle un contient une piece ennemie, on ajoute la case et on quitte la boucle
				if(!(cases.get(tempValue).getPieceActuelle().getCouleur()==this.getCouleur()))
					retList.add(tempValue);
				// Sinon, c'est une piece alliée, on ne peut aller sur case et on quitte la boucle 
				// sans ajouter la case
				break;
			}
			// Si la case est vide, on ajoute la case aux valeurs possibles
			else retList.add(tempValue);
		}
		
		tempValue = selectedCaseIntValue;
		
		// tant qu'on est pas sur le bord, on peut descendre
		while(!bottomBorder.contains(tempValue)) {
			// Si on arrive sur une case non vide
			if(cases.get(tempValue+=8).getPieceActuelle()!=null) {
				// Si elle un contient une piece ennemie, on ajoute la case et on quitte la boucle
				if(!(cases.get(tempValue).getPieceActuelle().getCouleur()==this.getCouleur()))
					retList.add(tempValue);
				// Sinon, c'est une piece alliée, on ne peut aller sur case et on quitte la boucle 
				// sans ajouter la case
				break;
			}
			// Si la case est vide, on ajoute la case aux valeurs possibles
			else retList.add(tempValue);
		}
		
		tempValue = selectedCaseIntValue;
		
		// tant qu'on est pas sur le bord, on peut aller a droite
		while(!rightBorder.contains(tempValue)) {
			// Si on arrive sur une case non vide
			if(cases.get(tempValue+=1).getPieceActuelle()!=null) {
				// Si elle un contient une piece ennemie, on ajoute la case et on quitte la boucle
				if(!(cases.get(tempValue).getPieceActuelle().getCouleur()==this.getCouleur()))
					retList.add(tempValue);
				// Sinon, c'est une piece alliée, on ne peut aller sur case et on quitte la boucle 
				// sans ajouter la case
				break;
			}
			// Si la case est vide, on ajoute la case aux valeurs possibles
			else retList.add(tempValue);
		}
		
		tempValue = selectedCaseIntValue;
		
		while(!leftBorder.contains(tempValue)) {
			// Si on arrive sur une case non vide
			if(cases.get(tempValue-=1).getPieceActuelle()!=null) {
				// Si elle un contient une piece ennemie, on ajoute la case et on quitte la boucle
				if(!(cases.get(tempValue).getPieceActuelle().getCouleur()==this.getCouleur()))
					retList.add(tempValue);
				// Sinon, c'est une piece alliée, on ne peut aller sur case et on quitte la boucle 
				// sans ajouter la case
				break;
			}
			// Si la case est vide, on ajoute la case aux valeurs possibles
			else retList.add(tempValue);
		}
		

				
		// On enlève les valeurs hors du plateau, utile uniquement pour l'IA
		for(int i=0;i<retList.size();++i)
			if(retList.get(i)<0 || retList.get(i)>55) {
				retList.remove(i);
				i=0;
			}
		return retList;
	}
}
