package Model;

import java.util.ArrayList;

public class Pion extends Piece{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4852654620157972769L;

	public Pion(String couleur, String nom) {
		
		super(couleur,nom);
		this.poids=10;
	}
	public ArrayList<Integer> getPath(int selectedCaseIntValue,  ArrayList<Case> cases){
		
		ArrayList<Integer> retList = new ArrayList<Integer>();
		
		// Dans le cas du joueur noir
		if (this.getCouleur().equals("noir")) {	
			// Ces valeurs ne doivent être ajouté que si on est pas sur le bord gauche
			if(!leftBorder.contains(selectedCaseIntValue)) {
				// Si la case a +7 n'est pas vide et contient une piece ennemie, on ajoute la valeur
				if(cases.get(selectedCaseIntValue+7).getPieceActuelle() !=null
					&& cases.get(selectedCaseIntValue+7).getPieceActuelle().getCouleur()!=this.getCouleur())
				retList.add(selectedCaseIntValue+7);
				
			}
			// Ces valeurs ne doivent être ajouté que si on est pas sur le bord droit
			if(!rightBorder.contains(selectedCaseIntValue)) {
				// Si la case a +9 n'est pas vide et contient une piece ennemie, on ajoute la valeur
				if(cases.get(selectedCaseIntValue+9).getPieceActuelle() !=null
					&& cases.get(selectedCaseIntValue+9).getPieceActuelle().getCouleur()!=this.getCouleur())
				retList.add(selectedCaseIntValue+9);
				
			}
			// On ne peut se déplacer tout droit que si le case est vide
			if(cases.get(selectedCaseIntValue+8).getPieceActuelle() ==null)
				retList.add(selectedCaseIntValue + 8);
				
		}
		else {
			// Ces valeurs ne doivent être ajouté que si on est pas sur le bord gauche
			if(!leftBorder.contains(selectedCaseIntValue)) {
				// Si la case a +7 n'est pas vide et contient une piece ennemie, on ajoute la valeur
				if(cases.get(selectedCaseIntValue-9).getPieceActuelle() !=null
					&& cases.get(selectedCaseIntValue-9).getPieceActuelle().getCouleur()!=this.getCouleur())
				retList.add(selectedCaseIntValue-9);
				
			}
			// Ces valeurs ne doivent être ajouté que si on est pas sur le bord droit
			if(!rightBorder.contains(selectedCaseIntValue)) {
				// Si la case a +9 n'est pas vide et contient une piece ennemie, on ajoute la valeur
				if(cases.get(selectedCaseIntValue-7).getPieceActuelle() !=null
					&& cases.get(selectedCaseIntValue-7).getPieceActuelle().getCouleur()!=this.getCouleur())
				retList.add(selectedCaseIntValue-7);
				
			}
			// On ne peut se déplacer tout droit que si le case est vide
			if(cases.get(selectedCaseIntValue-8).getPieceActuelle() ==null)
				retList.add(selectedCaseIntValue - 8);	
		}
		// On enlève les valeurs hors du plateau, utile uniquement pour l'IA
		for(int i=0;i<retList.size();++i)
			if(retList.get(i)<0 || retList.get(i)>55) {
				retList.remove(i);
				i=0;
			}
		return retList;
	}
}
