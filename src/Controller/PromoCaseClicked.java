package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Vue.CaseButton;
import Vue.PromoFrame;

public class PromoCaseClicked implements ActionListener{

	PromoFrame motherFrame;
	
	public PromoCaseClicked(PromoFrame motherFrame) {
		
		this.motherFrame=motherFrame;		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		Object obj = e.getSource();

		// Il n'y a que des case clickables dans la fenêtre donc on cast sans risque
		CaseButton caseClicked = (CaseButton)obj;
		
		// La pièce mere devient celle qu'on a choisi
		motherFrame.setPieceAPromo(caseClicked.getButtonID());
	}

}
