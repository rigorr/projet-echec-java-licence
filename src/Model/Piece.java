package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class Piece implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -263511368323797941L;
	protected String couleur;
	protected Case selfCase;
	protected String nom;
	protected int poids;
	
	protected ArrayList<Integer> leftBorder;
	protected ArrayList<Integer> rightBorder;
	protected ArrayList<Integer> topBorder; 
	protected ArrayList<Integer> bottomBorder; 

	public Piece(String couleur, String nom) {
		
		this.couleur=couleur;
		this.nom=nom;
		
		leftBorder = new ArrayList<Integer>();
		rightBorder = new ArrayList<Integer>();
		topBorder = new ArrayList<Integer>();
		bottomBorder = new ArrayList<Integer>();
		
		leftBorder.addAll(Arrays.asList(0, 8, 16, 24, 32, 40, 48)); 
		rightBorder.addAll(Arrays.asList(7, 15, 23, 31, 39, 47, 55));
		topBorder.addAll(Arrays.asList(0,1,2,3,4,5,6,7));
		bottomBorder.addAll(Arrays.asList(48,49,50,51,52,53,54,55));
		
	}
	
	public abstract ArrayList<Integer> getPath(int selectedCaseIntValue, ArrayList<Case> cases);	
	
	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	

	public Case getSelfCase() {
		return selfCase;
	}

	public void setSelfCase(Case selfCase) {
		this.selfCase = selfCase;
	}

	public String getName() {
		return nom;
	}

	public void setName(String nom) {
		this.nom = nom;
	}

	public int getPoids() {
		return poids;
	}

	public void setPoids(int poids) {
		this.poids = poids;
	}

	
	
}
