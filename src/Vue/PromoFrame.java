package Vue;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import Controller.PromoCaseClicked;
import Model.Case;
import Model.Cavalier;
import Model.Fou;
import Model.Reine;
import Model.Tour;

// Cette classe sert a gérer la promotion de pion dans une fenêtre à part
// Elle est a la fois Vue et Modèle dans son cycle, mais pour une interaction si minime
//  ca ne me parait pas judicieux de recréer un MVC complet
public class PromoFrame extends JDialog implements Runnable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4150419427304705623L;
	private PromoCaseClicked caseClickAction;
	private JPanel mainPanel;
	private Case pieceAPromo;
	private ArrayList<CaseButton> buttonList;
	private ArrayList<Case>cases;
	private Color lightBlue = new Color(173, 216, 230);
	private Color lightGray = new Color(213, 211, 211);
	
	public PromoFrame(Case pieceAPromo, String couleurJoueur, int dim) {
		
		super();
		this.setResizable(false);
		// On veut éviter d'agir sur le plateau pendant ce temps
		this.setModal(true);
		
		this.pieceAPromo=pieceAPromo;
		this.buttonList = new ArrayList<CaseButton>();
		this.cases = new ArrayList<Case>();
		
		mainPanel=new JPanel();
		mainPanel.setLayout(new GridLayout(0,4));		
		
		
		this.getContentPane().add(mainPanel);
		
	}

	public void showPromote() {
		
		caseClickAction = new PromoCaseClicked(this);

		//Même concept que pour le plateau de base, on créé des cases clickables
		//et on leur donne les pièces possible pour la promotion
		for (int i=0;i<4;++i) {
			CaseButton buttonIteration = new CaseButton(i);
			buttonIteration.setOpaque(true);
			
			if(i%2==0) {
				buttonIteration.setBackground(lightBlue);
				buttonIteration.setBaseColor(lightBlue);
			}
			else {
				buttonIteration.setBackground(lightGray);
				buttonIteration.setBaseColor(lightGray);
			}
			
			// On donne l'action
			buttonIteration.addActionListener(caseClickAction);
			// On l'ajoute a la liste de boutons et a la visu
			buttonList.add(buttonIteration);
			mainPanel.add(buttonIteration);
			
			Case caseIteration = new Case(i);
			if (i==0)
				caseIteration.setPieceActuelle(new Tour(pieceAPromo.getPieceActuelle().getCouleur(),"tour"));
			else if(i==1) 
				caseIteration.setPieceActuelle(new Fou(pieceAPromo.getPieceActuelle().getCouleur(),"fou"));
			else if(i==2) 
				caseIteration.setPieceActuelle(new Cavalier(pieceAPromo.getPieceActuelle().getCouleur(),"cavalier"));	
			else if(i==3) 
				caseIteration.setPieceActuelle(new Reine(pieceAPromo.getPieceActuelle().getCouleur(),"reine"));	
			
			cases.add(caseIteration);
		}
		
		// On set les images des pieces pour tout le monde
		for (int i=0;i<cases.size();++i)
			if(cases.get(i).getPieceActuelle()!=null) {
				setCaseIcon(cases.get(i));
				rePaint(cases.get(i),i);
			}
		
		
		this.pack();
		this.setVisible(true);
			
		
	}
	
	// On set les images
	public void setCaseIcon(Case caseToSet) {
		
		//On récupère l'image
		File imgFile = null;
			imgFile = new File(System.getProperty("user.dir")+"/images/"+caseToSet.getPieceActuelle().getName()+
					"_"+caseToSet.getPieceActuelle().getCouleur()+".png");
		BufferedImage img = null;
		try {
			img = ImageIO.read(imgFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//On stretch aux dimensions des cases -10 pour éviter de les faire coller aux bords
		Image tmpImg = img.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
		caseToSet.setIcon(new ImageIcon(tmpImg));
		
	}
	
	// Affichage 
	public void rePaint(Case caseToPaint,int i) {
		
		buttonList.get(i).setIcon(caseToPaint.getIcon());  
		if(caseToPaint.isSelected() || caseToPaint.isValidPlay())
			buttonList.get(i).setBackground(Color.GREEN);
		else buttonList.get(i).setBackground(buttonList.get(i).getBaseColor());
	}
		

	public void setPieceAPromo(int indice) {
		this.pieceAPromo = cases.get(indice);
		for(JButton button : buttonList)
			button.setEnabled(false);
	}

	public Case getPieceAPromo() {
		return pieceAPromo;
	}

	@Override
	public void run() {
		caseClickAction = new PromoCaseClicked(this);

		//Même concept que pour le plateau de base, on créé des cases clickables
		//et on leur donne les pièces possible pour la promotion
		for (int i=0;i<4;++i) {
			CaseButton buttonIteration = new CaseButton(i);
			buttonIteration.setOpaque(true);
			
			if(i%2==0) {
				buttonIteration.setBackground(lightBlue);
				buttonIteration.setBaseColor(lightBlue);
			}
			else {
				buttonIteration.setBackground(lightGray);
				buttonIteration.setBaseColor(lightGray);
			}
			
			// On donne l'action
			buttonIteration.addActionListener(caseClickAction);
			// On l'ajoute a la liste de boutons et a la visu
			buttonList.add(buttonIteration);
			mainPanel.add(buttonIteration);
			
			Case caseIteration = new Case(i);
			if (i==0)
				caseIteration.setPieceActuelle(new Tour(pieceAPromo.getPieceActuelle().getCouleur(),"tour"));
			else if(i==1) 
				caseIteration.setPieceActuelle(new Fou(pieceAPromo.getPieceActuelle().getCouleur(),"fou"));
			else if(i==2) 
				caseIteration.setPieceActuelle(new Cavalier(pieceAPromo.getPieceActuelle().getCouleur(),"cavalier"));	
			else if(i==3) 
				caseIteration.setPieceActuelle(new Reine(pieceAPromo.getPieceActuelle().getCouleur(),"reine"));	
			
			cases.add(caseIteration);
		}
		
		// On set les images des pieces pour tout le monde
		for (int i=0;i<cases.size();++i)
			if(cases.get(i).getPieceActuelle()!=null) {
				setCaseIcon(cases.get(i));
				rePaint(cases.get(i),i);
			}
		
		
		this.pack();
		this.setVisible(true);
			
		
	}


}
