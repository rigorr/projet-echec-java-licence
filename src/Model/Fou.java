package Model;

import java.util.ArrayList;

public class Fou extends Piece{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6272590900478440711L;

	public Fou(String couleur, String nom) {
		
		super(couleur,nom);
		this.poids=25;
	}

	public ArrayList<Integer> getPath(int selectedCaseIntValue, ArrayList<Case> cases){
		
		ArrayList<Integer> retList = new ArrayList<Integer>();
		
		int tempValue = selectedCaseIntValue;
		
		// tant qu'on est pas sur le bord, on peut monter a droite
		while(!topBorder.contains(tempValue) && !rightBorder.contains(tempValue)) {
			// Si on arrive sur une case non vide
			if(cases.get(tempValue-=7).getPieceActuelle()!=null) {
				// Si elle un contient une piece ennemie, on ajoute la case et on quitte la boucle
				if(!(cases.get(tempValue).getPieceActuelle().getCouleur()==this.getCouleur()))
					retList.add(tempValue);
				// Sinon, c'est une piece alliée, on ne peut aller sur case et on quitte la boucle 
				// sans ajouter la case
				break;
			}
			// Si la case est vide, on ajoute la case aux valeurs possibles
			else retList.add(tempValue);
		}
		
		
		tempValue = selectedCaseIntValue;
		
		// tant qu'on est pas sur le bord, on peut descendre a droite
		while(!bottomBorder.contains(tempValue) && !rightBorder.contains(tempValue)) {
			// Si on arrive sur une case non vide
			if(cases.get(tempValue+=9).getPieceActuelle()!=null) {
				// Si elle un contient une piece ennemie, on ajoute la case et on quitte la boucle
				if(!(cases.get(tempValue).getPieceActuelle().getCouleur()==this.getCouleur()))
					retList.add(tempValue);
				// Sinon, c'est une piece alliée, on ne peut aller sur case et on quitte la boucle 
				// sans ajouter la case
				break;
			}
			// Si la case est vide, on ajoute la case aux valeurs possibles
			else retList.add(tempValue);
		}
		
		tempValue = selectedCaseIntValue;
		
		// tant qu'on est pas sur le bord, on peut monter a gauche
		while(!topBorder.contains(tempValue) && !leftBorder.contains(tempValue)){
			// Si on arrive sur une case non vide
			if(cases.get(tempValue-=9).getPieceActuelle()!=null) {
				// Si elle un contient une piece ennemie, on ajoute la case et on quitte la boucle
				if(!(cases.get(tempValue).getPieceActuelle().getCouleur()==this.getCouleur()))
					retList.add(tempValue);
				// Sinon, c'est une piece alliée, on ne peut aller sur case et on quitte la boucle 
				// sans ajouter la case
				break;
			}
			// Si la case est vide, on ajoute la case aux valeurs possibles
			else retList.add(tempValue);
		}
		
		tempValue = selectedCaseIntValue;
		
		// tant qu'on est pas sur le bord, on peut descendre a gauche
		while(!bottomBorder.contains(tempValue) && !leftBorder.contains(tempValue)){
			// Si on arrive sur une case non vide
			if(cases.get(tempValue+=7).getPieceActuelle()!=null) {
				// Si elle un contient une piece ennemie, on ajoute la case et on quitte la boucle
				if(!(cases.get(tempValue).getPieceActuelle().getCouleur()==this.getCouleur()))
					retList.add(tempValue);
				// Sinon, c'est une piece alliée, on ne peut aller sur case et on quitte la boucle 
				// sans ajouter la case
				break;
			}
			// Si la case est vide, on ajoute la case aux valeurs possibles
			else retList.add(tempValue);
		}
		return retList;
	}
	
}
