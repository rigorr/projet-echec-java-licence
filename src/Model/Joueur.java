package Model;

import java.io.Serializable;
import java.util.ArrayList;

public class Joueur implements Serializable, Runnable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7018715124969970889L;
	protected String couleur;
	protected boolean isActive;
	protected ArrayList<Piece> piecesJoueur;
	protected int difficulty;
	
	public Joueur() {}
	
	public Joueur(String couleurJoueur) {
		
		couleur=couleurJoueur;
		piecesJoueur = new ArrayList<Piece>();
	}
	
	public ArrayList<Piece> getPiecesJoueur() {
		return piecesJoueur;
	}


	public void setPiecesJoueur(ArrayList<Piece> piecesJoueur) {
		this.piecesJoueur = piecesJoueur;
	}


	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	
	public int getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	public void run() {};

}
