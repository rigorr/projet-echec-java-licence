package Vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import Controller.JMenuEvent;
import Controller.PlateauCaseClick;
import Model.Case;
import Model.Joueur;
import Model.Model;

public class MainFrame extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8806744154987414607L;
	private JMenu menu;
	private JMenuBar menubar;
	private JMenuItem save, load, newGameBeginner, newGameEasy, newGameIntermediate;
	private JPanel mainPanel, greatPanel;
	private SidePanel sidePanel;
	private ArrayList<CaseButton> buttonList;
	private Color lightBlue = new Color(173, 216, 230);
	private Color lightGray = new Color(213, 211, 211);
	private EndFrame ef;
	private Model mainModel;
	
	public MainFrame(String name) throws IOException {
			
		super(name);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Création de fenêtre fin de jeu
		ef = new EndFrame();
		
		mainModel = new Model(this);
		//Création des éléments de la vue
		greatPanel = new JPanel();
		mainPanel = new JPanel();
		sidePanel = new SidePanel(this);
		
		buttonList = new ArrayList<CaseButton>();
		PlateauCaseClick caseClickAction = new PlateauCaseClick(mainModel);
		//Grille d'échec de 7 par 8
		mainPanel.setLayout(new GridLayout(7,8));
		mainPanel.setPreferredSize(new Dimension(800,600));
		
		//Utilisé pour l'inversion des couleurs de case
		boolean invert=false;
		
		for (int i=0;i<7*8;++i) {
				
			CaseButton buttonIteration = new CaseButton(i);
			buttonIteration.addActionListener(caseClickAction);
			
			buttonIteration.setOpaque(true);

			// Tous les 8 cases, on inverse l'alternance de couleur
			if (i%8==0)
				invert=!invert;
			
			// On colorie les cases
			if (invert) {
				if (i%2==0) {
					buttonIteration.setBackground(lightBlue);
					buttonIteration.setBaseColor(lightBlue);
				}
				else {
					buttonIteration.setBackground(lightGray);
					buttonIteration.setBaseColor(lightGray);
				}
			}
			else {
				if (i%2==0) {
					buttonIteration.setBackground(lightGray);
					buttonIteration.setBaseColor(lightGray);
				}
				else {
					buttonIteration.setBackground(lightBlue);
					buttonIteration.setBaseColor(lightBlue);
				}
			}
			
			buttonList.add(buttonIteration);
			mainPanel.add(buttonIteration);
		}
		
		
	    // Créer le menu
	    menubar = new JMenuBar();
	    menu = new JMenu("Options");
	    // Créer les éléments du menu
	    newGameBeginner = new JMenuItem("Partie débutant");
	    newGameEasy = new JMenuItem("Partie facile");
	    newGameIntermediate = new JMenuItem("Partie intermédiaire");
	    save = new JMenuItem("Sauvegarder");
	    load = new JMenuItem("Charger");
	    
	    
	    JMenuEvent menuEvent = new JMenuEvent(mainModel, this);
	    newGameBeginner.addActionListener(menuEvent);
	    newGameEasy.addActionListener(menuEvent);
	    newGameIntermediate.addActionListener(menuEvent);
		save.addActionListener(menuEvent);
		load.addActionListener(menuEvent);
		
	    // Ajouter les éléments au menu
		menu.add(newGameBeginner);
		menu.add(newGameEasy);
		menu.add(newGameIntermediate);
	    menu.add(save); 
	    menu.add(load); 
	    // Ajouter le menu au barre de menu
	    menubar.add(menu);
	    this.setJMenuBar(menubar);
	    // On ajoute tout a la frame
	    greatPanel.add(mainPanel);
	    greatPanel.add(sidePanel);
		this.getContentPane().add(greatPanel);

		this.pack();
		this.setVisible(true);
	}
		
	
	public void rePaint(Case caseToPaint,int i) {
		
		buttonList.get(i).setIcon(caseToPaint.getIcon());  
		if(caseToPaint.isSelected() || caseToPaint.isValidPlay())
			buttonList.get(i).setBackground(Color.GREEN);
		else buttonList.get(i).setBackground(buttonList.get(i).getBaseColor());
	}
	
	public void endGame(Joueur joueurGagnant) {
		
		// On affiche le gagnant
		ef.endGame(joueurGagnant);
		// Disable les buttons pour éviter de continuer a jouer
		for (CaseButton button : buttonList)
			button.setEnabled(false);
	}
	

	public SidePanel getSidePanel() {
		return sidePanel;
	}

	

	public ArrayList<CaseButton> getButtonList() {
		return buttonList;
	}


	public void setButtonList(ArrayList<CaseButton> buttonList) {
		this.buttonList = buttonList;
	}

	

	public Model getMainModel() {
		return mainModel;
	}


	public void createNewModel() {
		this.mainModel =new Model(this);
	}


	
	public void setMainModel(Model mainModel) {
		this.mainModel = mainModel;
	}


	public static void main(String [] args) {

		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	try {
					new MainFrame("Echec POO");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
	    });
	}	

}
