package Model;

import java.util.ArrayList;
import java.util.Arrays;

public class Cavalier extends Piece{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1233914888094727874L;

	private ArrayList<Integer> secondColumn;
	private ArrayList<Integer> seventhColumn;
	
	public Cavalier(String couleur, String nom) {
		
		super(couleur,nom);
		this.poids=25;
		
		secondColumn = new ArrayList<Integer>();
		seventhColumn = new ArrayList<Integer>();
		
		secondColumn.addAll(Arrays.asList(1, 9, 17, 25, 33, 41, 49)); 
		seventhColumn.addAll(Arrays.asList(6, 14, 22, 30, 38, 46, 54));
	}
	
	public ArrayList<Integer> getPath(int selectedCaseIntValue, ArrayList<Case> cases){
		
		ArrayList<Integer> retList = new ArrayList<Integer>();
		
		// Ces valeurs ne doivent être ajouté que si on est pas sur le bord gauche
		if (!leftBorder.contains(selectedCaseIntValue)) {
			retList.add(selectedCaseIntValue + 15);
			retList.add(selectedCaseIntValue - 17);
			//Pareil pour la colonne 2
			if(!secondColumn.contains(selectedCaseIntValue)){
				retList.add(selectedCaseIntValue - 10);
				retList.add(selectedCaseIntValue + 6);
			}
		}
		// Ces valeurs ne doivent être ajouté que si on est pas sur le bord droit
		if ( !rightBorder.contains(selectedCaseIntValue) ) {
			retList.add(selectedCaseIntValue - 15);
			retList.add(selectedCaseIntValue + 17);
			//Pareil pour la colonne 6
			if(!seventhColumn.contains(selectedCaseIntValue)){
				retList.add(selectedCaseIntValue + 10);
				retList.add(selectedCaseIntValue - 6);	
			}
		}
				
		// On enlève les valeurs hors du plateau, utile uniquement pour l'IA
		for(int i=0;i<retList.size();++i)
			if(retList.get(i)<0 || retList.get(i)>55) {
				retList.remove(i);
				i=0;
			}
		return retList;
	}
}
