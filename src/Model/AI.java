package Model;

import java.util.ArrayList;
import java.util.Random;

public class AI extends Joueur implements Runnable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5160947246717528758L;
	private Model model;
	private ArrayList<Integer> possibleMoves;
	private Random rn=new Random();
	//private ArrayList<SimulatedTurn> possiblePlats;
	private ArrayList<SimulatedTurn> possiblePlatsDeep;
	private Case useCase;
	private Joueur otherPlayer;
	private boolean playRandomTurn = true;
	private int turnToPlay, highestPoids,tmpPoids;
	
	
	public AI(String couleurJoueur,Model model, Joueur otherPlayer) {
		
		super(couleurJoueur);
		this.model=model;
		this.otherPlayer = otherPlayer;
		possibleMoves = new ArrayList<Integer>();
		//possiblePlats= new ArrayList<SimulatedTurn>();
		possiblePlatsDeep = new ArrayList<SimulatedTurn>();
	}
	
	

	public Model getModel() {
		return model;
	}



	public void setModel(Model model) {
		this.model = model;
	}



	@Override
	public void run() {
		if(difficulty == 1)
			playRandomTurn();
		else if(difficulty == 2)
			playDeep1Turn();
		else if(difficulty == 3) {
			playSimTurn();
		}	
	}		

	
	private void playRandomTurn() {
		
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		boolean hasPlayed=false;
		while(possibleMoves.size()==0) {
			//On prend une pièce au hasard
			useCase = this.getPiecesJoueur().get(rn.nextInt(this.getPiecesJoueur().size())).selfCase;
			// On va chercher tous les mouvements possibles pour la pièce
			possibleMoves=useCase.getPieceActuelle().getPath(useCase.getCaseID(), model.getCases());
			
			// On retire de la liste des mouvements possibles les cases ou il y a une pièce de la même couleur que nous
			for(int k=0;k<possibleMoves.size();) {
				if((model.getCases().get(possibleMoves.get(k)).getPieceActuelle()!=null) &&
				(model.getCases().get(possibleMoves.get(k)).getPieceActuelle().getCouleur()==piecesJoueur.get(k).getCouleur())) {
					if(possibleMoves.size()==1)
						possibleMoves.clear();
					else possibleMoves.remove(k);
					k=0;
				}
				else ++k;
			}
		}
		
		// On demande a la vue de cliquer sur le bouton correspondant a la case choisie
		model.getMf().getButtonList().get(useCase.getCaseID()).doClick();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		while(!hasPlayed) {
			// On prend une des possibilités au hasard
			int caseIdToClick = rn.nextInt(possibleMoves.size());
			
			//et on appel click dessus
			for (Case useCase2 : model.getCases())
				if(Integer.valueOf(useCase2.getCaseID())==possibleMoves.get(caseIdToClick) /*&& 
				( useCase2.getPieceActuelle()==null || useCase2.getPieceActuelle().getCouleur()!=useCase.getPieceActuelle().getCouleur())*/) {
					model.getMf().getButtonList().get(useCase2.getCaseID()).doClick();
					possibleMoves.clear();
					hasPlayed=true;
					break;
				}
		}
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void playSimTurn() {
		possiblePlatsDeep.clear();
		for(int i=0;i<piecesJoueur.size();++i) {
			// On récupère toutes les possibilités de déplacement de la pièce
			possibleMoves= piecesJoueur.get(i).getPath(piecesJoueur.get(i).getSelfCase().getCaseID(), model.getCases());
			// On retire de la liste des mouvements possibles les cases ou il y a une pièce de la même couleur que nous
			for(int k=0;k<possibleMoves.size();) {
				if((model.getCases().get(possibleMoves.get(k)).getPieceActuelle()!=null) &&
				(model.getCases().get(possibleMoves.get(k)).getPieceActuelle().getCouleur()==piecesJoueur.get(i).getCouleur())) {
					if(possibleMoves.size()==1)
						possibleMoves.clear();
					else possibleMoves.remove(k);
					k=0;
				}
				else ++k;
			}
			// Pas besoin de continuer si aucun déplacement possible
			if(possibleMoves.size()>0) {
				for(int j=0;j<possibleMoves.size();++j) {
					// Ici je suis obligé de prendre chaque case du model précédent et de les cloner
					// car sinon je ne fais que passer des références, et cloner des listes en profondeur est
					// un peu plus compliqué que prévu
					 ArrayList<Case> casesSimulate = new ArrayList<Case>();
					 for(Case baseCase : model.getCases())
						 casesSimulate.add(new Case(baseCase));
					
					// On déplace la pièce si le mouvement est légal
					if(model.mouvementLegal(piecesJoueur.get(i),
							casesSimulate.get(possibleMoves.get(j)),
							casesSimulate.get(piecesJoueur.get(i).getSelfCase().getCaseID()))) {
						casesSimulate.get(possibleMoves.get(j)).setPieceActuelle(piecesJoueur.get(i));
						casesSimulate.get(piecesJoueur.get(i).getSelfCase().getCaseID()).setPieceActuelle(null);
						
						// On crée un "SimulatedTurn", qui permet d'avoir les mouvement effectués,
						// et le poids du nouveau plateau
						SimulatedTurn T = new SimulatedTurn(casesSimulate);
						T.setFirstClick(piecesJoueur.get(i).getSelfCase().getCaseID());
						T.setSecondClick(possibleMoves.get(j));
						T.setPoids(model.calculPoids(casesSimulate, this));
						
						// On enregiste tous les coups ennemis possibles pour le coup que l'on vient de faire
						getEnnemyTurns(otherPlayer, casesSimulate, T);				

						// Ajout de ces possibilités à la liste
						possiblePlatsDeep.add(T);
					}
				}
			}
		}
		tmpPoids=0;
		// On prend arbitrairement le premier poids puisque l'on va
		// tout comparer. Si on prend 0 l'algo ne marche plus pour un
		// poids négatif
		highestPoids=possiblePlatsDeep.get(0).getEnnemyMoves().get(0).getPoids();
		turnToPlay=0;
		playRandomTurn=true;
		for(int i=0;i<possiblePlatsDeep.size();++i) {
			for(int j=0;j<possiblePlatsDeep.get(i).getEnnemyMoves().size();++j) {
				tmpPoids = possiblePlatsDeep.get(i).getEnnemyMoves().get(j).getPoids();
				if(tmpPoids>highestPoids) {
					highestPoids=tmpPoids;
					turnToPlay=i;
					// Si on passe au moins une fois ici tous les poids ne sont pas égaux
					playRandomTurn=false;
				}					
			}		
		}
		
		// Si le poids de tous les plateaux potentiels sont identiques (comme en début de partie)
		// on joue un coup au hasard
		if(playRandomTurn)
			turnToPlay = rn.nextInt(possiblePlatsDeep.size());
		
		//  et on effectue les deux déplacements stockés dans celui-ci
		model.getMf().getButtonList().get(possiblePlatsDeep.get(turnToPlay).getFirstClick()).doClick();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		model.getMf().getButtonList().get(possiblePlatsDeep.get(turnToPlay).getSecondClick()).doClick();
	}
	
	public void getEnnemyTurns(Joueur J, ArrayList<Case> cases, SimulatedTurn baseTurn) {
		
		ArrayList<Integer> possibleEnnemyMoves = new ArrayList<Integer>();
		
		for(int i=0;i<J.getPiecesJoueur().size();++i) {
			// On récupère toutes les possibilités de déplacement de la pièce
			possibleEnnemyMoves= J.getPiecesJoueur().get(i).getPath(J.getPiecesJoueur().get(i).getSelfCase().getCaseID(), cases);
			// On retire de la liste des mouvements possibles les cases ou il y a une pièce de la même couleur que nous
			for(int k=0;k<possibleEnnemyMoves.size();) {
				if((cases.get(possibleEnnemyMoves.get(k)).getPieceActuelle()!=null) &&
				(cases.get(possibleEnnemyMoves.get(k)).getPieceActuelle().getCouleur()==J.getPiecesJoueur().get(i).getCouleur())) {
					if(possibleEnnemyMoves.size()==1)
						possibleEnnemyMoves.clear();
					else possibleEnnemyMoves.remove(k);
					k=0;
				}
				else ++k;
			}
			// Pas besoin de continuer si aucun déplacement possible
			if(possibleEnnemyMoves.size()>0) {
				for(int j=0;j<possibleEnnemyMoves.size();++j) {
					// Ici je suis obligé de prendre chaque case du model précédent et de les cloner
					// car sinon je ne fais que passer des références, et cloner des listes en profondeur est
					// un peu plus compliqué que prévu
					 ArrayList<Case> casesSimulate = new ArrayList<Case>();
					 for(Case baseCase : cases)
						 casesSimulate.add(new Case(baseCase));
					
					// On déplace la pièce si le mouvement est légal
					if(model.mouvementLegal(J.getPiecesJoueur().get(i),
							casesSimulate.get(possibleEnnemyMoves.get(j)),
							casesSimulate.get(J.getPiecesJoueur().get(i).getSelfCase().getCaseID()))) {
						casesSimulate.get(possibleEnnemyMoves.get(j)).setPieceActuelle(J.getPiecesJoueur().get(i));
						casesSimulate.get(J.getPiecesJoueur().get(i).getSelfCase().getCaseID()).setPieceActuelle(null);
						
						// On crée un "SimulatedTurn", qui permet d'avoir les mouvement effectués,
						// et le poids du nouveau plateau
						SimulatedTurn T = new SimulatedTurn(casesSimulate);
						T.setFirstClick(J.getPiecesJoueur().get(i).getSelfCase().getCaseID());
						T.setSecondClick(possibleEnnemyMoves.get(j));
						T.setPoids(model.calculPoids(casesSimulate, this));
						
						baseTurn.getEnnemyMoves().add(T);
						
					}
				}
			}
		}
	}
	
	// Fonctionnel mais pas encore très intelligent, cherche juste à capturer la pièce la plus
	// importante de l'ennemi, facilement trompé
	public void playDeep1Turn() {
		
		ArrayList<SimulatedTurn> possiblePlats = new ArrayList<SimulatedTurn>();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for(int i=0;i<piecesJoueur.size();++i) {
			// On récupère toutes les possibilités de déplacement de la pièce
			possibleMoves= piecesJoueur.get(i).getPath(piecesJoueur.get(i).getSelfCase().getCaseID(), model.getCases());
			// On retire de la liste des mouvements possibles les cases ou il y a une pièce de la même couleur que nous
			for(int k=0;k<possibleMoves.size();) {
				if((model.getCases().get(possibleMoves.get(k)).getPieceActuelle()!=null) &&
				(model.getCases().get(possibleMoves.get(k)).getPieceActuelle().getCouleur()==piecesJoueur.get(i).getCouleur())) {
					if(possibleMoves.size()==1)
						possibleMoves.clear();
					else possibleMoves.remove(k);
					k=0;
				}
				else ++k;
			}
			// Pas besoin de continuer si aucun déplacement possible
			if(possibleMoves.size()>0) {
				for(int j=0;j<possibleMoves.size();++j) {
					// Ici je suis obligé de prendre chaque case du model précédent et de les cloner
					// car sinon je ne fais que passer des références, et cloner des listes en profondeur est
					// un peu plus compliqué que prévu
					 ArrayList<Case> casesSimulate = new ArrayList<Case>();
					 for(Case baseCase : model.getCases())
						 casesSimulate.add(new Case(baseCase));
					
					// On déplace la pièce si le mouvement est légal
					if(model.mouvementLegal(piecesJoueur.get(i),
							casesSimulate.get(possibleMoves.get(j)),
							casesSimulate.get(piecesJoueur.get(i).getSelfCase().getCaseID()))) {
						casesSimulate.get(possibleMoves.get(j)).setPieceActuelle(piecesJoueur.get(i));
						casesSimulate.get(piecesJoueur.get(i).getSelfCase().getCaseID()).setPieceActuelle(null);
						
						// On crée un "SimulatedTurn", qui permet d'avoir les mouvement effectués,
						// et le poids du nouveau plateau
						SimulatedTurn T = new SimulatedTurn(casesSimulate);
						T.setFirstClick(piecesJoueur.get(i).getSelfCase().getCaseID());
						T.setSecondClick(possibleMoves.get(j));
						T.setPoids(model.calculPoids(casesSimulate, this));
						
						possiblePlats.add(T);
					}
				}
			}
		}
		
		turnToPlay=0;
		// On prend arbitrairement le premier poids puisque l'on va
		// tout comparer. Si on prend 0 l'algo ne marche plus pour un
		// poids négatif
		highestPoids=possiblePlats.get(0).getPoids();
		tmpPoids=0;
		playRandomTurn=true;
		// On récupère l'indice du plateau le plus avantageux
		for(int i=1;i<possiblePlats.size();++i) {
			tmpPoids=possiblePlats.get(i).getPoids();
			if(tmpPoids>highestPoids) {
				highestPoids=tmpPoids;
				turnToPlay=i;
			}
		}
		
		// Y'a surement plus élégant, mais ici on veut déterminer si toutes les valeurs
		//  de poids sont égales, car dans ce cas il faut choisir au hasard plutot que de jouer
		// toujours le premier coup possible
		for(int i=0;i<possiblePlats.size();++i) {
			if(possiblePlats.get(0).getPoids()!=possiblePlats.get(i).getPoids()) {
				playRandomTurn=false;
			}
		}
		
		// Si le poids de tous les plateaux potentiels sont identiques (comme en début de partie)
		// on joue un coup au hasard
		if(playRandomTurn)
			turnToPlay = rn.nextInt(possiblePlats.size());
			
		//  et on effectue les deux déplacements stockés dans celui-ci
		model.getMf().getButtonList().get(possiblePlats.get(turnToPlay).getFirstClick()).doClick();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		model.getMf().getButtonList().get(possiblePlats.get(turnToPlay).getSecondClick()).doClick();
		
	}

	// **************************** NON FONCTIONNEL ****************************
	//****************************************************************************
	/*
	public void playDeepBlueTurn(ArrayList<Case> baseCases, Joueur J) {
		
		ArrayList<SimulatedTurn> possiblePlats = new ArrayList<SimulatedTurn>();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for(int i=0;i<J.getPiecesJoueur().size();++i) {
			// On récupère toutes les possibilités de déplacement de la pièce
			possibleMoves= J.getPiecesJoueur().get(i).getPath(J.getPiecesJoueur().get(i).getSelfCase().getCaseID(), baseCases);
			// On retire de la liste des mouvements possibles les cases ou il y a une pièce de la même couleur que nous
			for(int k=0;k<possibleMoves.size();) {
				if((baseCases.get(possibleMoves.get(k)).getPieceActuelle()!=null) &&
				(baseCases.get(possibleMoves.get(k)).getPieceActuelle().getCouleur()==J.getPiecesJoueur().get(i).getCouleur())) {
					if(possibleMoves.size()==1)
						possibleMoves.clear();
					else possibleMoves.remove(k);
					k=0;
				}
				else ++k;
			}
			// Pas besoin de continuer si aucun déplacement possible
			if(possibleMoves.size()>0) {
				for(int j=0;j<possibleMoves.size();++j) {
					// Ici je suis obligé de prendre chaque case du model précédent et de les cloner
					// car sinon je ne fais que passer des références, et cloner des listes en profondeur est
					// un peu plus compliqué que prévu
					 ArrayList<Case> casesSimulate = new ArrayList<Case>();
					 for(Case baseCase : baseCases)
						 casesSimulate.add(new Case(baseCase));
					
					// On déplace la pièce si le mouvement est légal
					if(model.mouvementLegal(J.getPiecesJoueur().get(i),
							casesSimulate.get(possibleMoves.get(j)),
							casesSimulate.get(J.getPiecesJoueur().get(i).getSelfCase().getCaseID()))) {
						casesSimulate.get(possibleMoves.get(j)).setPieceActuelle(J.getPiecesJoueur().get(i));
						casesSimulate.get(J.getPiecesJoueur().get(i).getSelfCase().getCaseID()).setPieceActuelle(null);
						
						// On crée un "SimulatedTurn", qui permet d'avoir les mouvement effectués,
						// et le poids du nouveau plateau
						SimulatedTurn T = new SimulatedTurn(casesSimulate);
						T.setFirstClick(J.getPiecesJoueur().get(i).getSelfCase().getCaseID());
						T.setSecondClick(possibleMoves.get(j));
						

						
						//Tours possibles de l'autre joueur
						for(int m=0;m<otherPlayer.getPiecesJoueur().size();++m) {
							possibleMoves.clear();
							possibleMoves= otherPlayer.getPiecesJoueur().get(m).getPath(otherPlayer.getPiecesJoueur().get(m).getSelfCase().getCaseID(), casesSimulate);
							// On retire de la liste des mouvements possibles les cases ou il y a une pièce de la même couleur que nous
							for(int k=0;k<possibleMoves.size();) {
								if((model.getCases().get(possibleMoves.get(m)).getPieceActuelle()!=null) &&
								(model.getCases().get(possibleMoves.get(k)).getPieceActuelle().getCouleur()==otherPlayer.getPiecesJoueur().get(m).getCouleur())) {
									if(possibleMoves.size()==1)
										possibleMoves.clear();
									else possibleMoves.remove(k);
									k=0;
								}
								else ++k;
							}
							// Pas besoin de continuer si aucun déplacement possible
							if(possibleMoves.size()>0) {
								for(int n=0;n<possibleMoves.size();++n) {
									// Ici je suis obligé de prendre chaque case du model précédent et de les cloner
									// car sinon je ne fais que passer des références, et cloner des listes en profondeur est
									// un peu plus compliqué que prévu
									 ArrayList<Case> otherCasesSimulate = new ArrayList<Case>();
									 for(Case baseCase : casesSimulate)
										 otherCasesSimulate.add(new Case(baseCase));
									 
									 if(model.mouvementLegal(piecesJoueur.get(m),
											 otherCasesSimulate.get(possibleMoves.get(n)),
											 otherCasesSimulate.get(piecesJoueur.get(m).getSelfCase().getCaseID()))) {
										 otherCasesSimulate.get(possibleMoves.get(n)).setPieceActuelle(piecesJoueur.get(m));
										 otherCasesSimulate.get(piecesJoueur.get(m).getSelfCase().getCaseID()).setPieceActuelle(null);
											
											T.getEnnemyMoves().add(otherCasesSimulate);
									 }
									 
								}
							}
							
						}
						int tempPoids;
						for(int c=0;c<T.getEnnemyMoves().size();++c) {
							tempPoids = model.calculPoids(T.getEnnemyMoves().get(c), this);
							if(tempPoids > highestPoids) {
								highestPoids=tempPoids;
								turnToPlay=c;
							}
						}
						
						
						
						possiblePlats.add(T);
					}
				}
			}
		}
		
		turnToPlay=0;
		highestPoids=0;
		playRandomTurn=true;
		// On récupère l'indice du plateau le plus avantageux
		for(int p=0;p<possiblePlats.size();++p) {
			if(possiblePlats.get(p).getPoids()>highestPoids) {
				turnToPlay=p;
			}
		}
		
		// Y'a surement plus élégant, mais ici on veut déterminer si toutes les valeurs
		//  de poids sont égales, car dans ce cas il faut choisir au hasard plutot que de jouer
		// toujours le premier coup possible
		for(int i=0;i<possiblePlats.size();++i) {
			if(possiblePlats.get(0).getPoids()!=possiblePlats.get(i).getPoids()) {
				playRandomTurn=false;
			}
		}
		
		// Si le poids de tous les plateaux potentiels sont identiques (comme en début de partie)
		// on joue un coup au hasard
		if(playRandomTurn)
			turnToPlay = rn.nextInt(possiblePlats.size());
		
		possiblePlatsDeep.add(possiblePlats.get(turnToPlay));
		
		++depth;
		if(depth<4) {
		// On doit calculer 1 fois sur 2 le meilleur coup pour l'IA puis pour le joueur pour décider
			if(invert) {
				invert=!invert;
				playDeepBlueTurn(possiblePlats.get(turnToPlay).getPlat(), otherPlayer);
			}
			else playDeepBlueTurn(possiblePlats.get(turnToPlay).getPlat(), this);
		}
		
		//  et on effectue les deux déplacements stockés dans celui-ci
		model.getMf().getButtonList().get(possiblePlats.get(turnToPlay).getFirstClick()).doClick();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		model.getMf().getButtonList().get(possiblePlats.get(turnToPlay).getSecondClick()).doClick();
			
		possiblePlatsDeep.clear();
		
		
	}

*/
}
